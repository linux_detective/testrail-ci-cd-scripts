#!/bin/bash 

set -e

BRANCHNAME=$1

echo "Source remote git repo:"

source <(curl -s https://raw.githubusercontent.com/MaksymSemenykhin/bash_scripts/master/output.sh)

if [[ ${BRANCHNAME} =~ ^(develop|master)$ ]]; then
   print_title "Select branch: $BRANCHNAME"
   else
   print_error "ERROR, Script stoped."
   fi 

# Prereg..
DEPLOY_FOLDER="/var/www/nodes/$BRANCHNAME"
mkdir -p "DELPOY_FOLDER"
print_title "Deploy folder: $DEPLOY_FOLDER"


# Deploy part

print_info "Start deploy part"

mkdir -p "$DEPLOY_FOLDER/config"
cp ./config/"$BRANCHNAME".json "$DEPLOY_FOLDER"/config/local.config.json

sed -i s#%ENV%#$BRANCHNAME#g "$DEPLOY_FOLDER/ecosystem.config.json"
print_info "Print ecosystem js file:"
cat ecosystem.config.json

print_info "Copy app to $DEPLOY_FOLDER"
cp -r . "$DEPLOY_FOLDER"

print_info "pm2 version: $(pm2 --version)"
pm2 Start

# Test
print_info "Start unit && functial test part"
npm run test
